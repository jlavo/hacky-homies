import numpy as np
import timezonefinder
from timezonefinder import TimezoneFinder
import geopy
from  geopy.geocoders import Nominatim
import pytz, datetime
import math
import matplotlib.pyplot as plt
import vpython
from vpython import *

#Get date and city input

valid_date = False
while valid_date == False:
    try:
        date = input("What day would you like to visit? YYYY-MM-DD \n")

        #Store date in variables
        year = int(date[0:4])
        month = int(date[5:7])
        day= int(date[8:])            
        pytz.datetime.datetime(year,month,day)      
        valid_date = True
    except:
        print("\n Please enter a valid date!")
    #finally: continue

valid_location = False
while valid_location == False:
    try:
        location = input("What city would you like to visit? City-Country \n")

        #Store city and country in variables
        city =""
        country =""
        
        for i in range(len(location)):
            if location[i] != '-':
                city = city+location[i]
            else:
                country = country+location[i+1:]
                break
        
        #Gives latitude/longitude of a city
        geolocator = Nominatim(user_agent="Hacky_Homies")
        loc = geolocator.geocode(city+','+ country)

        latitude = loc.latitude
        longitude = loc.longitude
        valid_location = True
    except:
        print("\n Please enter a valid city and country!")
    finally: continue

#Tells you the timezone you're in given latitude/longitude, as a string (ex: 'America/Toronto')
tf = TimezoneFinder()
time_zone = tf.timezone_at(lng=longitude, lat=latitude)

#Gives time difference between time_zone and UTC, output is a string (ex: '+0500')
GMT = pytz.timezone(time_zone).localize(datetime.datetime(year,month,day)).strftime('%z')

#To get output GMT as an integer time difference from UTC (or 'GMT') as an integer
if GMT[3] == '0':
    GMT = int(GMT[:3])
else: GMT = int(GMT[:4])
print("\nGMT value is ", GMT, " in ", city+', '+country)

day = datetime.datetime(year,month,day).timetuple().tm_yday

#################### KATIE'S CODE ############################

local_time_array = np.linspace(0,24,1000) #local time, midnight to midnight in a day

#renaming functions and putting in degrees
def cos(x):
    return math.cos(math.radians(x))
def sin(x):
    return math.sin(math.radians(x))
def acos(x):
    return math.acos(x)
def asin(x):
    return math.asin(x)

def sun_positions(latitude,longitude,GMT,day):
    
    "put in some checks for correct values at one point"
    
    
    """
    function that takes in parameters:
            latitude: in degrees (-90 to 90)
            longitude: in degrees (-180 to 180)
            GMT: Grenwich Mean Time offset (time zone) ie.+6. Can take on positive or negative values. Hours.
            day: number of days since new year (Jan 1st=0)
    Calculates the position of the sun from 12am to 12pm on a given day.
    Output :
        An array of 100 positions in the sky, given as pairs of [elevation,azimuth] in degrees
    """
    
    LSTM = 15*(GMT) #local standard time meridian
    
    B=(360.0/365.0)*(day-81)
    
    EOT = 9.87*sin(2*B)-7.53*cos(B)-1.5*sin(B) #equation of time, minutes
    
    TC = 4*(longitude-LSTM)+EOT #time correction factor, minutes
    
    
    positions = []
    
    for LT in local_time_array:
        LST = LT+(TC/60.0) #Local solar time
        HRA = 15*(LST-12)#hour angle
        declination = 23.45*sin((360.0/365.0)*(day-81))
        elevation = asin((sin(declination)*sin(latitude))+(cos(declination)*cos(latitude)*cos(HRA)))
        azimuth = acos(((sin(declination)*cos(latitude))-(cos(declination)*sin(latitude)*cos(HRA)))/cos(elevation))
        positions.append([elevation,azimuth])

    #need to convert output into cartesian coordinates !!
    return positions



""" tester code """

positions = sun_positions(latitude,longitude,GMT,day)

elevations = [x[0] for x in positions]
azimuths = [x[1] for x in positions]

plt.plot(local_time_array,azimuths,label="azimuth")
plt.plot(local_time_array,elevations,label="elevation")
plt.legend()



########################### JO'S CODE ################################

scene.title = "A Sunflower Sundial"
scene.background = color.gray(0.7)

#size of screen and axes
L = 50
scene.center = vec(0.05*L,0.2*L,0)
scene.range = 1.3*L
R = L/100
d = L-2
k = 1.02
h = 0.05*L

#sunflower
#bar1 = box(pos=vector(0,-6,0),size = vector(1,10,1),color=color.green,texture=textures.rough)
#flowerTop = sphere(pos=vector(0,0,0), size = vector(4,4,4),color = color.yellow,texture=textures.stucco)


#-------------------------------------------------------------------------------------------------
##Sunflower Graphics

bar1 = box(pos=vector(0,-6,0),size = vector(1,10,1),color=color.green)
flowerTop = sphere(pos=vector(0,0,0), size = vector(4,4,4),color = color.orange)
flowerback = sphere(pos=vector(0.05,0,0.1), size = vector(4,4,4),color = color.green)


s1 = sphere(pos=vector(-2.1,-0.8,0),radius=0.5,color=color.yellow)
c13 = cone(pos=vector(-2.1,-0.8,0),axis=vector(2,0,0),radius=0.55,color = color.yellow)
c13.rotate(angle=pi+0.2,axis=vec(0,0,1))

#left
s2 = sphere(pos=vector(-2.5,0,0),radius=0.5,color=color.yellow)
c = cone(pos=vector(-2.5,0,0),axis=vector(2,0,0),radius=0.55,color = color.yellow)
c.rotate(angle=pi,axis=vec(0,0,1))

s3 =sphere(pos=vector(-2.2,1,0),radius=0.5,color=color.yellow)
c1 = cone(pos=vector(-2.2,1,0),axis=vector(2,0,0),radius=0.55,color = color.yellow)
c1.rotate(angle=pi-0.2,axis=vec(0,0,1))

s4 =sphere(pos=vector(-1.7,1.7,0),radius=0.5,color=color.yellow)
c2 = cone(pos=vector(-1.7,1.7,0),axis=vector(2,0,0),radius=0.55,color = color.yellow)
c2.rotate(angle=pi-0.5,axis=vec(0,0,1))

s5=sphere(pos=vector(-1.2,2,0),radius=0.5,color=color.yellow)
c3 = cone(pos=vector(-1.2,2,0),axis=vector(2,0,0),radius=0.55,color = color.yellow)
c3.rotate(angle=pi-0.8,axis=vec(0,0,1))

s6=sphere(pos=vector(-0.6,2.3,0),radius=0.5,color=color.yellow)
c4 = cone(pos=vector(-0.6,2.3,0),axis=vector(2,0,0),radius=0.55,color = color.yellow)
c4.rotate(angle=pi-1.2,axis=vec(0,0,1))

#top
s7=sphere(pos=vector(0,2.5,0),radius=0.5,color=color.yellow)
c5 = cone(pos=vector(0,2.5,0),axis=vector(2,0,0),radius=0.55,color = color.yellow)
c5.rotate(angle=pi/2,axis=vec(0,0,1))

s8 = sphere(pos=vector(0.8,2.3,0),radius=0.5,color=color.yellow)
c6 = cone(pos=vector(0.8,2.3,0),axis=vector(2,0,0),radius=0.55,color = color.yellow)
c6.rotate(angle=pi/2-0.2,axis=vec(0,0,1))

s9=sphere(pos=vector(1.4,2,0),radius=0.5,color=color.yellow)
c7 = cone(pos=vector(1.4,2,0),axis=vector(2,0,0),radius=0.55,color = color.yellow)
c7.rotate(angle=pi/2-0.3,axis=vec(0,0,1))

s10=sphere(pos=vector(1.8,1.5,0),radius=0.5,color=color.yellow)
c8 = cone(pos=vector(1.8,1.5,0),axis=vector(2,0,0),radius=0.55,color = color.yellow)
c8.rotate(angle=pi/2-0.7,axis=vec(0,0,1))

s11=sphere(pos=vector(2.2,0.75,0),radius=0.5,color=color.yellow)
c9 = cone(pos=vector(2.2,0.75,0),axis=vector(2,0,0),radius=0.55,color = color.yellow)
c9.rotate(angle=pi/2-0.9,axis=vec(0,0,1))

#right
s12=sphere(pos=vector(2.5,0,0),radius=0.5,color=color.yellow)
c10 = cone(pos=vector(2.5,0,0),axis=vector(2,0,0),radius=0.55,color = color.yellow)

s12=sphere(pos=vector(2.2,-0.55,0),radius=0.5,color=color.yellow)
c11 = cone(pos=vector(2.2,-0.55,0),axis=vector(2,0,0),radius=0.55,color = color.yellow)
c11.rotate(angle=-0.15*pi/4,axis=vec(0,0,1))

s13=sphere(pos=vector(1.9,-1.05,0),radius=0.5,color=color.yellow)
c12 = cone(pos=vector(1.9,-1.05,0),axis=vector(2,0,0),radius=0.55,color = color.yellow)
c12.rotate(angle=-0.2*pi,axis=vec(0,0,1))

    
#bottomRight
#sphere(pos=vector(1.2,-2.2,0),radius=0.5,color=color.yellow)
#c = cone(pos=vector(1.2,-2.2,0),axis=vector(2,0,0),radius=0.55,color = color.yellow)
#c.rotate(angle=-0.4*pi,axis=vec(0,0,1))



flower = compound([flowerTop, flowerback,c, c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13])
#-------------------------------------------------------------------------------------------

#starting elevation and azymuth to sun cartesian coords
phi = (pi/2)-elevations[0]
theta = azimuths[0]
rho = 20
x_sun = rho*cos(theta)*sin(phi)
z_sun = rho*sin(theta)*sin(phi)
y_sun = rho*cos(phi)

#sun
b2 = sphere(radius=1, pos=vector(x_sun, y_sun, z_sun), color=color.yellow, emissive=True)
l2 = local_light(pos=b2.pos, color=b2.color)
    


for i in range(0, len(elevations)):
    
    #speed of rotation
    rate(1)
    
    #night and day
    if (l2.pos.y>=0):
        scene.background = color.cyan
    else : 
        scene.background = color.black
        
    #elevation and azymuth to sun cartesian coords
    phi = (pi/2)-elevations[i]
    theta = azimuths[i]
    x_sun = rho*cos(theta)*sin(phi)
    if (elevations[i]<elevations[i-1]):
        z_sun = -rho*sin(theta)*sin(phi)
    else: 
        z_sun = rho*sin(theta)*sin(phi)
    y_sun = rho*cos(phi)
    
    #rotate flower
    flower.rotate(angle=(azimuths[i-1]-azimuths[i]), axis=vec(0,1,0))
    flower.rotate(angle=((pi/2)-elevations[i-1])-((pi/2)-elevations[i]), axis=vec(1,0,0))
    
    #rotate sun
    l2.pos = b2.pos = vector(x_sun, y_sun, z_sun)







