#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov  7 20:53:54 2020

@author: katiesavard
"""

import numpy as np
import math
import matplotlib.pyplot as plt

local_time_array = np.linspace(0,24,1000) #local time, midnight to midnight in a day

#renaming functions and putting in degrees
def cos(x):
    return math.cos(math.radians(x))
def sin(x):
    return math.sin(math.radians(x))
def acos(x):
    return math.acos(x)
def asin(x):
    return math.asin(x)

def sun_positions(latitude,longitude,GMT,day):
    
    "put in some checks for correct values at one point"
    
    
    """
    function that takes in parameters:
            latitude: in degrees (-90 to 90)
            longitude: in degrees (-180 to 180)
            GMT: Grenwich Mean Time offset (time zone) ie.+6. Can take on positive or negative values. Hours.
            day: number of days since new year (Jan 1st=0)
    Calculates the position of the sun from 12am to 12pm on a given day.
    Output :
        An array of 100 positions in the sky, given as pairs of [elevation,azimuth] in degrees
    """
    
    LSTM = 15*(GMT) #local standard time meridian
    
    B=(360.0/365.0)*(day-81)
    
    EOT = 9.87*sin(2*B)-7.53*cos(B)-1.5*sin(B) #equation of time, minutes
    
    TC = 4*(longitude-LSTM)+EOT #time correction factor, minutes
    
    
    positions = []
    
    for LT in local_time_array:
        LST = LT+(TC/60.0) #Local solar time
        HRA = 15*(LST-12)#hour angle
        declination = 23.45*sin((360.0/365.0)*(day-81))
        elevation = asin((sin(declination)*sin(latitude))+(cos(declination)*cos(latitude)*cos(HRA)))
        azimuth = acos(((sin(declination)*cos(latitude))-(cos(declination)*sin(latitude)*cos(HRA)))/cos(elevation))
        positions.append([elevation,azimuth])

    #need to convert output into cartesian coordinates !!
    return positions



""" tester code """

positions = sun_positions(90,0,-6,10)

elevations = [x[0] for x in positions]
azimuths = [x[1] for x in positions]

plt.plot(local_time_array,azimuths,label="azimuth")
plt.plot(local_time_array,elevations,label="elevation")
plt.legend()

