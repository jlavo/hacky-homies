Welcome to The Sunflower Sundial! 

1) Introduction

Travel to any place in the world, on any day you want, and watch a sunflower delicately follow the sun as it glides through the sky. The sun's trajectory is dependent on the latitude and longitude (timezone) of the flower, as well as the day of the year.

2) Physics Concepts

- Solar declination
- Elevation, azimuth of the sun 

3) Important Notes

A few packages need to be installed:
- "conda install -c vpython vpython"
- "conda install geopy"
- "conda install Nominatim"
- "conda install timezonefinder"
- "conda install pytz"
